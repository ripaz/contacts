<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SaveContact extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname' => 'required|max:100',
            'lastname'  => 'required|max:100',
            'email'     => 'required|max:100',
            'picture'   => 'image',
            'favorite'  => 'boolean',
            'numbers'   => 'array',
            'phonenumbers'  => 'array|nullable',
            'phonenumbers.*'  => 'array',
            'phonenumbers.*.number' => [
                'max:100',
                'required_with:numbers.*.type',
                'required',
                'phone'
            ],
            'phonenumbers.*.type' => [
                'max:50',
                'required_with:numbers.*.number',
                'required',
                'string'
            ]
        ];
    }

    protected function prepareForValidation()
    {
        // NOTE Cast empty into array, because formdata can't send empty array
        // We require array so we know all numbers where deleted
        if ($this->has('phonenumbers')) {
            if (empty($this->input('phonenumbers'))) {
                $this->merge(['phonenumbers' => array()]);
            }
        }

        // NOTE can't send boolean trought formdata, so we convert them
        if ($this->has('favorite')) {

            $favorite = null;
            if (in_array($this->input('favorite'), ['1', 'true'])) {
                $favorite = true;
            }
            if (in_array($this->input('favorite'), ['0', 'false'])) {
                $favorite = false;
            }

            $this->merge(['favorite' => $favorite]);
        }
    }
}
