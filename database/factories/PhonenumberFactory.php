<?php

use Faker\Generator as Faker;
use App\Contact;

$factory->define(App\Phonenumber::class, function (Faker $faker) {

    return [
        'contact_id' => null,
        'number'     => $faker->e164PhoneNumber,
        'type'       => function () {
            $type_enums = ['landline', 'mobile'];
            $inx = rand(0, (count($type_enums) - 1));

            return $type_enums[$inx];
        }
    ];
});
