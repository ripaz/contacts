
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

// require('./bootstrap');
window.Vue = require('vue');
import VueRouter from 'vue-router';

let urlPath = document.head.querySelector('meta[name="url-path"]');

if (urlPath) {
  window.urlPath     = urlPath.content;
  window.apiPath     = urlPath.content + '/api/contacts';
} else {
  console.error('URL Path not found');
}

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


// 1. Use plugin.
// This installs <router-view> and <router-link>,
// and injects $router and $route to all router-enabled child components
window.Vue.use(VueRouter)

// 2. Define route components
import ContactList   from './components/contacts/list.vue';
import ContactSingle   from './components/contacts/single.vue';

// 3. Create the router
const router = new VueRouter({
  mode: 'history',
  base: '/contacts/public/', // FIXME add dynamic
  routes: [
    {
      path: '/',
      component: ContactList,
      name: 'home'
    },
    {
      path: '/create',
      component: ContactSingle,
      name: 'create'
    },
    {
      path: '/:id',
      component: ContactSingle,
      name: 'show'
    },
    {
      path: '/:id/edit',
      component: ContactSingle,
      name: 'edit'
    },
  ]
})

// 4. Create and mount root instance.
// Make sure to inject the router.
// Route components will be rendered inside <router-view>.
const App = new Vue({
  router
}).$mount('#app');
