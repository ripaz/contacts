<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Is this really necessary? Maybe not but its a good measure
Route::middleware('throttle:60,1')->group(function () {
    Route::resource('contacts', 'ContactController');
});

Route::any('{all}', function () {
    abort(404);
})
->where(['all' => '.*']);
