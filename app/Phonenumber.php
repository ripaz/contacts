<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Phonenumber extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'contact_id',
		'number',
		'type',
    ];

    public function contact()
    {
        return $this->belongsTo('App\Contact');
    }

}
