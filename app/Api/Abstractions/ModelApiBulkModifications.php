<?php

namespace App\Api\Abstractions;

use Illuminate\Database\Eloquent\Model;

abstract class ModelApiBulkModifications
{

	protected static $relationshipName;

    /**
    *   @param  array 
    *   @return object this instance
    */
    public static function process(array $relationshipItems, Model $model) : void
    {
        $updatedCreated    		= self::updateOrCreate($relationshipItems, $model);
		$relationshipPrimaryKey = self::getRelationshipPrimaryKey($model);
		$updatedCreatedIds 		= array_column($updatedCreated, $relationshipPrimaryKey);
		
		self::deleteNotUpdatedOrCreated($updatedCreatedIds, $model);
    }
	
	/**
	 * @param Model $model Laravel model.
	 *
	 * @return string
	 */
	public static function getRelationshipPrimaryKey(Model $model) : string
	{
		return $model->{static::$relationshipName}()->getLocalKeyName();
	}
	
	public static function updateOrCreate(array $relationshipItems, Model $model) : array
	{
		$updatedCreated = array();
		
		
	    foreach ($relationshipItems as $phonenumber) {
	        $updatedCreated[] = $model->{static::$relationshipName}()->updateOrCreate($phonenumber);
	    }
		
		return $updatedCreated;
	}

    /**
    *   @param array Phonenumber array
    */
    public static function deleteNotUpdatedOrCreated(array $updatedCreatedIds, Model $model) : void
    {
		$relationshipPrimaryKey = self::getRelationshipPrimaryKey($model);
		
        $model->{static::$relationshipName}()
		->whereNotIn($relationshipPrimaryKey, $updatedCreatedIds)
		->delete();
    }
	
}
