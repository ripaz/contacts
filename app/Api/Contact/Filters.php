<?php

namespace App\Api\Contact;

use App\Api\Contact\Adapter as ContactAdapter;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Filters
{
	
	protected static $methodPrefix = 'filter';

    /**
    *   Filter by accepted filters
    *   @param Request
    *   @param object Instance
    */
    public static function filter(Request $request)
    {
        $instance = (new ContactAdapter)->newQuery();
        $appliedFilters = array();
		
		$filters = self::getFilterNames();
		
        foreach ($filters as $filter) {
            if ($request->has(mb_strtolower($filter))) {
				self::{self::$methodPrefix . $filter}($instance, $request->{mb_strtolower($filter)});
                $appliedFilters[$filter] = $request->{$filter};
            }
        }

        return $instance->paginate(12)->appends($appliedFilters);
    }
	
	public static function getFilterNames()
	{
		$class   = get_class();
		$methods = get_class_methods($class);
		
		$filterMethods = array_filter($methods, function ($method) {
			return strpos($method, self::$methodPrefix) === 0;
		});
		
		return array_map([$class, 'removeScopeFromString'], $filterMethods);
		
	}
	
	public static function removeScopeFromString(string $string) : string
	{
		return str_replace(self::$methodPrefix, '', $string);
	}

    /**
    *   @param Query
    *   @param string Search keywords
    */
    public static function filterSearch($query, $value)
    {
        $pieces = array_filter(explode(' ', trim($value)));

        foreach ($pieces as $piece) {
            $query->where(function ($subQuery) use ($piece) {
                $subQuery
                ->where('firstname', 'like', '%' . $piece . '%')
                ->orWhere('lastname', 'like', '%' . $piece . '%')
                ->orWhere('email', 'like', '%' . $piece . '%')
                ->orWhereHas('phonenumbers', function ($relQuery) use ($piece) {
                    $relQuery
                    ->where('number', 'like', '%' . $piece . '%')
                    ->orWhere('type', 'like', '%' . $piece . '%');
                });
            });
        }

        return $query;
    }

    /**
    *   @param Query
    *   @param string
    */
    public static function filterGroups($query, $value)
    {
        if ($value === 'favorites') {
            $query->where('favorite', true);
        }

        return $query;
    }
}
