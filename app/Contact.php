<?php

namespace App;

use App\Api\Contact\ProfilePicture;
use App\Api\Contact\Phonenumbers;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

class Contact extends Model
{
	
	protected $table = 'contacts';
	
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname',
		'lastname',
		'email',
		'favorite',
		'picture',
    ];

    /**
     * I guess these are default values if none exist?
     *
     * @var array
     */
    protected $attributes = [
        'favorite' => false
    ];
	
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];


    public function phonenumbers()
    {
        return $this->hasMany('App\Phonenumber', 'contact_id');
    }

    public static function boot()
    {
        parent::boot();

        self::deleted(function ($Contact) {
            $Contact->phonenumbers()->delete();
            ProfilePicture::remove($Contact->picture);
        });

        self::updating(function ($Contact) {
            // Delete old image if uploading new
			$original = $Contact->getOriginal('picture');
			if ($original && $original !== $Contact->picture) {
				ProfilePicture::remove($original);
			}
        });
    }

}
