<?php

namespace App\Contact;

use App\Contact;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Illuminate\Http\UploadedFile;
use Illuminate\Http\Request;

class ProfilePicture
{
    // On what disk we are putting pictures
    protected static $storageDrive = 'public';
    // Directory where our pictures are saved
    protected static $storagePath = 'profile_pictures';
	
    /**
    *   Attempt to upload picture,
    *   @param object Request object
    *   @return object this instance
    *   / null on empty file input
    */
    public static function processUploadRequest(Request $request, Contact $contact, string $field = 'picture')
    {
        if ($request->hasFile($field)) {
            $file = $request->file($field);

            $contact->picture = self::uploadAndResize($file);
        } else if (empty($_FILES[$field]) === false) {
			// hasFile always return NULL on empty file, not matter if field exists
			// or not, this is a fix for it
			
            // When an empty file input is present, it means we removed old
            // image, but haven't uploaded new
            $contact->picture = null;
        }
		
		$contact->save();
		
    }

	public static function uploadAndResize(UploadedFile $file) : string
	{
		$path = self::upload($file);
		
		if ($path) {
			self::resize($path);
		}
		
		return $path;
	}

    public static function upload(UploadedFile $file)
    {
        return $file->isValid() ? $file->store(self::$storagePath, self::$storageDrive) : null;
    }

    /**
    *   @param string Path to our image
    */
    public static function remove(string $picturePath) : void
    {
		self::getPictureStorageDisk()->delete($picturePath);
    }

    /**
    *   Resize our picture to a propper dimensions
    *   @param string Picture path
    */
    public static function resize(string $picturePath) : void
    {
		$storage = self::getPictureStorageDisk();

        if ($storage->exists($picturePath)) {
            $imageFile = $storage->get($picturePath);
            $resized = Image::make($imageFile)->fit(400, 400)->encode();
            $storage->put($picturePath, $resized);
        }

    }

    /**
    *   @return Storage disk instance
    */
    public static function getPictureStorageDisk()
    {
        return Storage::disk(self::$storageDrive);
    }

}
