import axios from 'axios'

export const api = {
  methods: {
    get client () {
      return axios.create({
        baseURL: window.apiPath,
        json: true
      })
    },
    async execute (method, resource, data) {
      // inject the accessToken for each request
      let config = {
        method,
        url: resource
      }

      if (data) {
        for (let item in data) {
          config[item] = data[item]
        }
      }

      return this.client(config).then(req => {
        return req.data
      }).catch(error => {
        if (error.response.data.errors) {
          this.errors = error.response.data.errors
        }
      })
    },
    getContacts (data) {
      return this.execute('get', '/', {params : data})
    },
    getContact (id) {
      return this.execute('get', '/' + id)
    },
    createContact (data) {
      return this.execute('post', '', {data : data})
    },
    updateContact (id, data) {
      // File upload is not possible with PUT method, so we use _method field
      data.append('_method', 'PUT')
      return this.execute('post', '/' + id, {data : data})
    },
    deleteContact (id) {
      return this.execute('delete', '/' + id)
    }
  },
  mounted () {
    console.log('API mounted');
  }
}
