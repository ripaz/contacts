## Start up

I could do image uploading in three different types:
1. Create form and append image to form and upload it as whole (bad for debugging, simple to interpret)
2. Send one request with JSON and other as POST data with image (good for debugging + decoupling, bad = 2 requests)
3. Encrypt image (eg. base64) and send JSON (good for testing, bad for browser and server processing)

I choose No. 1, because I wanted one request and at the time it seemed a good choice. Now I would probably go with no. 2.

Edit database connection and create new database

Do some magic!
```
composer init-project
```

Done!
