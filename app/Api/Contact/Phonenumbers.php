<?php

namespace App\Api\Contact;

use App\Api\Abstractions\ModelApiBulkModifications;

class Phonenumbers extends ModelApiBulkModifications
{

	static protected $relationshipName = 'phonenumbers';
	
}
