<?php

namespace App\Http\Controllers;
// include composer autoload

use App\Api\Contact\Adapter as ContactAdapter;
use App\Api\Contact\Filters as ContactFilters;
use Illuminate\Http\Request;
use App\Http\Requests\SaveContact;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        // For headless web app
        header('Access-Control-Allow-Origin: *');
    }

    public function index(Request $request)
    {
        return ContactFilters::filter($request);
    }

    /**
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SaveContact $request)
    {
        $Contact = (new ContactAdapter)::store($request);

        return response()->json($Contact->getAttributes(), 201);
    }

    /**
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return ContactAdapter::with('phonenumbers')->findOrFail($id);
    }

    /**
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SaveContact $request, $id)
    {
        ContactAdapter::store($request, $id);

        return response()->json($this->show($id), 200);
    }

    /**
     * @param  int  $id
     * @return 404 / 204
     */
    public function destroy($id)
    {
        ContactAdapter::findOrFail($id)->delete();

        return response()->json(null, 204); // Succes, but nothing to return
    }
}
