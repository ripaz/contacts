<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ContactsTableSeeds extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Contact::class, 5)->create()->each(function ($contacts) {
            $this->phones($contacts);
        });
    }

    public function phones($contact)
    {
        factory(App\Phonenumber::class, rand(1, 3))->create(array(
            'contact_id' => $contact->id
        ));
    }
}
