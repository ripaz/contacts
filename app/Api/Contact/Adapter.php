<?php

namespace App\Api\Contact;

use App\Api\Contact\ProfilePicture;
use App\Api\Contact\Phonenumbers;
use App\Contact;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

class Adapter extends Contact
{

    /**
     * The attributes that are dynamically added to response.
     *
     * @var array
     */
    protected $appends = [
		'pictureUrl'
	];

    /**
    *   Construct image URL path so we know where it is and not pass all
    *   path to view
    *   @return string/null url to image or NULL
    */
    public function getPictureUrlAttribute()
    {
        if (empty($this->attributes['picture']) === false) {
            return url(Storage::url($this->attributes['picture']));
        }

        return null;
    }

    /**
    *   Process create/update request
    *   @param Request
    *   @param int Contact ID
    *   @return object This instance
    */
    public static function store(Request $request, $id = null) : Contact
    {
        $Contact = self::updateOrCreate(['id' => $id], $request->all());
		
        ProfilePicture::processUploadRequest($request, $Contact);
		
		if ($request->has('phonenumbers')) {
			Phonenumbers::process($request->phonenumbers, $Contact);
		}

        $Contact->save();

        return $Contact;
    }

}
